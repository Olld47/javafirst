package course16;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class CodeSearch {
    public static void main(String[] args) throws Exception{
        FileInputStream in = new FileInputStream(new File("F:/mywork/gaode.xlsx"));

        Workbook workbook = new XSSFWorkbook(in);

        Sheet sheet = workbook.getSheetAt(0);

        int rowNum = sheet.getLastRowNum();
        Map<String,City> map = new HashMap<>();
        for (
                int i = 2;
                i < rowNum; i++)
        {
            City city =new City();
            Row row = sheet.getRow(i);
            String name = row.getCell(0).getStringCellValue();
            String abcode = row.getCell(1).getStringCellValue();
            city.setName(name);
            city.setAbcode(abcode);
            map.put(city.getAbcode(),city);
            //System.out.println(map.get(abcode).getName());
            //System.out.println(map.get(abcode).getAbcode());
        }

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String string = null;
        System.out.println("输出邮编查询城市");
        System.out.println("输入 结束 退出程序");
        do {
            try {
                string = bufferedReader.readLine();
                if (string.equals("结束"))
                {}
                else if (!map.containsKey(string)) {
                    System.out.println("未找到该该邮编所对应的城市，请输入正确的邮编");
                }
                else {
                    City city1 = map.get(string);
                    String youbian = city1.getAbcode();
                    char [] ch=youbian.toCharArray();
                    String yier =String.valueOf(ch[0])+ch[1];
                    String sansi = String.valueOf(ch[2])+ch[3];
                    String wuliu = String.valueOf(ch[4])+ch[5];
                    String zhixia = yier+"00"+"00";
                    //String zhixiashiqu = yier+"01"+wuliu;
                    City zhixiashi =map.get(zhixia);
                    //如果邮编是直辖市
                    if (yier.equals("11")||yier.equals("12")||yier.equals("31")||yier.equals("50")){
                        if (sansi.equals("00")) {
                            System.out.println("直辖市的名称是：" + city1.getName());
                            System.out.println("城市的邮编是：" + city1.getAbcode());
                            System.out.println("它下辖的区有： ");
                            //遍历直辖市下辖的区
                            for (int i =0;i<99;i++){
                                //str  等于 i的值，如果i<10 str = 0i
                                String str = String.valueOf(i);

                                if (i<10){
                                    str="0"+i;
                                }
                                //qu 直辖市下辖区的邮编
                                String qu = yier+"01"+str;
                                //重庆市下辖部分区的邮编
                                String qu2 = yier+"02"+str;
                                //直辖市下辖区对应的城市
                                City city2 = map.get(qu);
                                City city3 = map.get(qu2);
                                //城市的非空判断
                                if (city2!=null) {
                                    System.out.println("区的名称是：" + city2.getName());
                                    System.out.println("区的邮编是：" + city2.getAbcode());
                                    System.out.println("    ");
                                }
                                //重庆市部分区县的邮编对应的城市不为空
                                else if (city3!=null){
                                    System.out.println("区的名称是：" + city3.getName());
                                    System.out.println("区的邮编是：" + city3.getAbcode());
                                    System.out.println("    ");
                                }
                            }
                        }
                        //如果是直辖市下辖区的邮编
                        else if (sansi.equals("01")||sansi.equals("02")){
                            System.out.println("区的名称是："+city1.getName());
                            System.out.println("区的邮编是："+city1.getAbcode());
                            System.out.println("它属于的直辖市是："+zhixiashi.getName());
                        }
                    }
                    //特别行政区的查询
                    else if (yier.equals("81")||yier.equals("82")) {
                        if (wuliu.equals("00")) {
                            System.out.println("特别行政区的名称是：" + city1.getName());
                            System.out.println("城市的邮编是：" + city1.getAbcode());
                            System.out.println("它下辖的区有： ");

                            //遍历特别行政区下辖的区
                            for (int i =1;i<99;i++) {
                                //str  等于 i的值，如果i<10 str = 0i
                                String str = String.valueOf(i);

                                if (i < 10) {
                                    str = "0" + i;
                                }
                                String qu = yier+"00"+str;
                                City city2 = map.get(qu);
                                //城市的非空判断
                                if (city2!=null) {
                                    System.out.println("区的名称是：" + city2.getName());
                                    System.out.println("区的邮编是：" + city2.getAbcode());
                                    System.out.println("    ");
                                }
                            }
                        }
                        //如果是特别行政区下辖区的邮编
                        else if (sansi.equals("00")){
                            System.out.println("区的名称是："+city1.getName());
                            System.out.println("区的邮编是："+city1.getAbcode());
                            System.out.println("它属于的特别行政区是："+zhixiashi.getName());
                        }


                    }
                    //省份的查询
                    else {
                        if (sansi.equals("00")&&wuliu.equals("00")){
                            System.out.println("省/自治区的名称是：" + city1.getName());
                            System.out.println("省的邮编是：" + city1.getAbcode());
                            System.out.println("它下辖的地级市有： ");
                            for (int i =1;i<99;i++) {
                                //str  等于 i的值，如果i<10 str = 0i
                                String str = String.valueOf(i);

                                if (i < 10) {
                                    str = "0" + i;
                                }
                                String shi = yier+str+"00";
                                City city =map.get(shi);
                                if (city!=null) {
                                    System.out.println("地级市的名称是：" + city.getName());
                                    System.out.println("城市的邮编是：" + city.getAbcode());
                                    System.out.println("    ");
                                }
                            }
                        }else if (wuliu.equals("00")){
                            System.out.println("城市的名称是：" + city1.getName());
                            System.out.println("城市的邮编是：" + city1.getAbcode());
                            System.out.println("它下辖的区有： ");

                            //地级市下面城市的遍历
                            for (int i =1;i<99;i++) {
                                //str  等于 i的值，如果i<10 str = 0i
                                String str = String.valueOf(i);

                                if (i < 10) {
                                    str = "0" + i;
                                }
                                String xian = yier+"01"+str;
                                City city2 =map.get(xian);
                                //城市的非空判断
                                if (city2!=null) {
                                    System.out.println("区的名称是：" + city2.getName());
                                    System.out.println("区的邮编是：" + city2.getAbcode());
                                    System.out.println("    ");
                                }
                            }
                        }
                        //县的邮编
                        else {
                            String dijishi=yier+sansi+"00";
                            City shi = map.get(dijishi);

                            System.out.println("区的名称是："+city1.getName());
                            System.out.println("区的邮编是："+city1.getAbcode());
                            System.out.println("它属于的地级市是："+shi.getName());

                        }
                    }
                }
            }catch (Exception e) {
                e.printStackTrace();
            }
        }while (!string.equals("结束"));

    }
}
