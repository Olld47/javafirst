package course15;
import java.io.*;
public class fileoprea {
    public static void main(String[] args) throws Exception {
        File file = new File("D:/mywork/README.md");
        //创建FileOutPutStream对象，文件不存在则创建它
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        //构建OutputStreamWriter对象,参数可以指定编码
        OutputStreamWriter writer = new OutputStreamWriter(fileOutputStream,"UTF-8");
        //写入到缓冲区
        writer.append("这是我的工程文件夹，里面存放了我的工程，每一个文件夹都是一个单独的工程\r\n");
        writer.append("我在gitee上有他们的同名仓库，和他们一一对应，绑定\r\n");
        writer.append("使用git极大的提高了我学习编程的效率");
        //关闭写入流，同时会把缓冲区的内容写入文件，注销缓冲区
        writer.close();
        //关闭输出流，释放系统资源
        fileOutputStream.close();
        //创建FileInputStream对象
        FileInputStream fileInputStream = new FileInputStream(file);
        //创建InputStreamReader对象，编码和写入的相同
        InputStreamReader reader = new InputStreamReader(fileInputStream,"UTF-8");

        StringBuffer stringBuffer = new StringBuffer();
        while (reader.ready()){
            //转成char加到StringBuffer对象中
            stringBuffer.append((char)reader.read());
        }System.out.println(stringBuffer.toString());
        //关闭读取流
        reader.close();
        //关闭输入流，释放系统资源
        fileInputStream.close();

    }
}
