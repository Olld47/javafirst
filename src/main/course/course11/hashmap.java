package course11;

import java.util.HashMap;
import java.util.Map;

public class hashmap {
    public static void main(String[] args) {
        Map<String,Hero> hashmap1= new HashMap<String, Hero>();
        Map<String,Hero> hashmap2= new HashMap<String, Hero>();
        Map<String,Hero> hashmap3= new HashMap<String, Hero>();
        Hero liubei= new Hero("刘备");
        Hero zhangfei=new Hero("张飞");
        Hero guanyu=new Hero("关羽");
        Hero huamulan=new Hero("花木兰");
        Hero sulie=new Hero("苏烈");
        Hero baili=new Hero("百里守约");
        Hero daji=new Hero("妲己");
        Hero mengqi=new Hero("梦奇");
        Hero feiqin= new Hero("裴擒虎");
        //hashmap1 init
        hashmap1.put(liubei.getName(),liubei);
        hashmap1.put(zhangfei.getName(),zhangfei);
        hashmap1.put(guanyu.getName(),guanyu);

        //hashmap2
        hashmap2.put(huamulan.getName(),huamulan);
        hashmap2.put(sulie.getName(),sulie);
        hashmap2.put(baili.getName(),baili);

        //hashmap3
        hashmap3.put(daji.getName(),daji);
        hashmap3.put(mengqi.getName(), mengqi);
        hashmap3.put(feiqin.getName(),feiqin);
        //total
        Map<String,Map<String,Hero>> hashmap4=new HashMap<String, Map<String, Hero>>();
        hashmap4.put("三国",hashmap1);
        hashmap4.put("长城",hashmap2);
        hashmap4.put("妖怪",hashmap3);
        System.out.println(hashmap4.get("三国").get("刘备").getName());


    }
}
