package course12;

public class mistake {
    public static void Test1(int x) throws ArrayIndexOutOfBoundsException, ArithmeticException {
        System.out.println(x);
        if (x == 0) {
            System.out.println("没有异常");
        }
        //数组越界异常
        else if (x == 1) {
            int[] a = new int[3];
            a[3] = 5;
        }
        //算术异常
        else if (x == 2) {
            int i = 0;
            int j = 5 / 0;
        }
    }

    public static void main(String[] args) {
        try {
            Test1(0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            Test1(1);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("数组越界异常");
        }
        try {
            Test1(1);
        } catch (ArithmeticException e) {
            System.out.println("除数不能为0");
        }
        finally {
            System.out.println("lalala");
        }
        }
    }