package course14;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class ioTest2 {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String string = null;
        System.out.println("输入字符串");
        System.out.println("输入'叶冰'和'樊凡'或者'苏彤'退出程序");
        do{
            string = bufferedReader.readLine();
            System.out.println(string);
        }while(!string.equals("叶冰")&& !string.equals("樊凡")&&!string.equals("苏彤"));
    }
}
