package course14;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ioTest {
    public static void main(String[] args) throws IOException {
        char c;
        //创建这个对象后，我们可以使用read（）方法从控制台读取一个字符，或者用readLine（）方法读取一个字符串
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("输入字符，按下'q'键退出。");
        //读取字符
        do{
            c = (char)bufferedReader.read();
            System.out.println(c);
        }while (c!='q');

    }
}
