package course13;
import java.util.ArrayList;
import java.util.Iterator;
public class array {
    public static void main(String[] args) {
        ArrayList <String> list = new ArrayList<String>();
        // 增加元素到list对象中

        list.add("Item1");
        list.add("Item2");
        list.add(2, "Item3"); // 此条语句将会把“Item3”字符串增加到list的第3个位置。
        list.add("Item4");
        try {
            list.add(5,"Item5");
        }
        catch (IndexOutOfBoundsException e){
            System.out.println("数组添加的位置越界了");
            list.add("Item4");
        }
        finally {
            System.out.println("数组元素：");
            //遍历方法一：通过迭代器Iterator进行遍历
            Iterator iterator = list.iterator();
            while (iterator.hasNext()){
                String name = (String)iterator.next();
                System.out.println(name);
            }
        }
    }


}
